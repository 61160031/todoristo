import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/page/homepage.dart';
import 'package:todo_list/page/loginpage.dart';
import 'package:todo_list/provider/providers.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;
  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((user) {
      _navigatorKey.currentState!
          .pushReplacementNamed(user != null ? 'home' : 'login');
    });
  }

  @override
  void dispose() {
    super.dispose();
    _sub.cancel();
    super.dispose();
  }

  static final String title = 'Todo App';
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => TodosProvider(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: title,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            scaffoldBackgroundColor: Colors.blue[50],
          ),
          home: HomePage(),
          navigatorKey: _navigatorKey,
          initialRoute:
              FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
          onGenerateRoute: (setting) {
            switch (setting.name) {
              case 'home':
                return MaterialPageRoute(
                    settings: setting, builder: (_) => HomePage());
              case 'login':
                return MaterialPageRoute(
                    settings: setting, builder: (_) => LoginPage());
            }
          },
        ),
      );
}


// class MyApp extends StatelessWidget {
//   static final String title = 'Todo App';

//   @override
//   Widget build(BuildContext context) => ChangeNotifierProvider(
//         create: (context) => TodosProvider(),
//         child: MaterialApp(
//           debugShowCheckedModeBanner: false,
//           title: title,
//           theme: ThemeData(
//             primarySwatch: Colors.blue,
//             scaffoldBackgroundColor: Colors.blue[50],
//           ),
//           home: HomePage(),
//         ),
//       );
// }