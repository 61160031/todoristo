import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'model/todo.dart';

class FirebaseApi {

  static Future<String> createTodo(Todo todo) async {
    final docTask = FirebaseFirestore.instance.collection('todo_list').doc();

    todo.id = docTask.id;
    await docTask.set(todo.toJson());
    //print(todo.toJson().toString());
    return docTask.id;
  }

  static Future<void> updateToggleStatus(Todo todo) async {
    // final docTask = FirebaseFirestore.instance.collection('todo_list').doc();
    CollectionReference todosCollection =
        FirebaseFirestore.instance.collection('todo_list');
    //print(todo.toJson().toString());
    return todosCollection
        .doc(todo.id)
        .update(todo.toJson())
        .then((value) => print('Toggle Change'))
        .catchError((error) => print('Failed to update Change Toggle: $error'));
  }

  static Future<void> updateStatus(Todo todo) async {
    CollectionReference todosCollection =
        FirebaseFirestore.instance.collection('todo_list');
    //print(todo.toJson().toString());
    return todosCollection
        .doc(todo.id)
        .update(todo.toJson())
        .then((value) => print('Update todo'))
        .catchError((error) => print('Failed to update todo: $error'));
  }

  static Future<void> deleteTodo(Todo todo) async {
    CollectionReference todosCollection =
        FirebaseFirestore.instance.collection('todo_list');
    //print(todo.toJson().toString());
    return todosCollection
        .doc(todo.id)
        .delete()
        .then((value) => print('Delete todo'))
        .catchError((error) => print('Failed to Delete todo: $error'));
  }

}
