import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SnackBars {
  static void showSnackBar(BuildContext context, String text) =>
      Scaffold.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text(text)));

  static dynamic fromDateTimeToJson(DateTime createdTime) {
    if (createdTime == null) return null;

    return createdTime.toUtc();
  }
}
