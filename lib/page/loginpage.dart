import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    return await FirebaseAuth.instance.signInWithPopup(googleProvider);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.all(32),
            child: Column(children: [
              Text('Todoristo',style: TextStyle(fontSize: 30,color: Colors.blue),),
              Container(
                margin: EdgeInsets.all(5.0),
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage('/OIP.jpg'), fit: BoxFit.fill)),
              ),
              SizedBox(
                child: Text(''),
              ),
              ElevatedButton(
                  onPressed: () async {
                    await signInWithGoogle();
                  },
                  child: Text('Sign in with Google')),

              //SizedBox(),
            ]),
          ),
        ));
  }
}
