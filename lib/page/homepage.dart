import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/widget/add_todo.dart';
import 'package:todo_list/widget/complete_list_widget.dart';
import 'package:todo_list/widget/drawer_widget.dart';
import 'package:todo_list/widget/todo_list_widget.dart';
// import 'package:todo_list/widget/Todo_form.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final tabs = [
      ListWidget(),
      CompletedListWidget(),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text('Todoristo App'),
      ),
      drawer: DrawerWidget(),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).primaryColor,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white.withOpacity(0.5),
        currentIndex: selectedIndex,
        onTap: (index) => setState(() {
          selectedIndex = index;
        }),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.list_alt), label: 'To do'),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.done,
                size: 30,
              ),
              label: 'Completed'),
        ],
      ),
      body: tabs[selectedIndex],
      floatingActionButton: FloatingActionButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        backgroundColor: Colors.blue,
        onPressed: () => showDialog(
          context: context,
          builder: (context) {
            return AddTodoWidget();
            // AddTodoWidget();
          },
        ),
        child: Icon(Icons.add),
      ),
    );
  }
}
