import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerWidget extends StatefulWidget {
  DrawerWidget({Key? key}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  _DrawerWidgetState();
  String? _imageURL = null;
  String? _nameURL = null;
  final Stream<QuerySnapshot> _todoStream = FirebaseFirestore.instance
      .collection('todo_list')
      .where('isDone', isEqualTo: false)
      .snapshots();

  @override
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
      _nameURL = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
              decoration: BoxDecoration(color: Colors.blue[100]),
              child: Center(
                child: ClipOval(
                  child: Image.network(
                    _imageURL!,
                    width: 100,
                    height: 100,
                    fit: BoxFit.fill,
                  ),
                ),
              )),
          Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: [
                Text(
                  _nameURL!,
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  child: Text(''),
                ),
                ElevatedButton(
                    onPressed: () async {
                      await FirebaseAuth.instance.signOut();
                    },
                    child: Text('Sign Out')),
              ],
            ),
          )
        ],
      ),
    );
  }
}
